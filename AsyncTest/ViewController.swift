//
//  ViewController.swift
//  AsyncTest
//
//  Created by Josh Campion on 18/04/2016.
//  Copyright © 2016 Josh Campion Dev. All rights reserved.
//

import AsyncDisplayKit

class ViewController: ASViewController, ASTableDataSource {
    
    let rootNode = LinksNode()
    
    init() {
        
        super.init(node: rootNode)
        
        rootNode.tableNode.dataSource = self
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented. This is an ASDK view.")
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }

    func tableView(tableView: ASTableView, nodeForRowAtIndexPath indexPath: NSIndexPath) -> ASCellNode {
        
        return PageMetaDataNode(data: MZPageMetaData.TheDistanceMetaData())
    }
    
}

class LinksNode: ASDisplayNode {
    
    let tableNode = ASTableNode(style: .Grouped)
    let emptyNode = ASTextNode()
    let errorNode = ASTextNode()
    
    override init() {
        super.init()
        
        
        tableNode.backgroundColor = UIColor.whiteColor().colorWithAlphaComponent(0.25)
        backgroundColor = UIColor.groupTableViewBackgroundColor()
        
        emptyNode.attributedString = NSAttributedString(string: "No Links Found")
        errorNode.attributedString = NSAttributedString(string: "Error occured")
        
        
        addSubnode(tableNode)
        addSubnode(emptyNode)
        addSubnode(errorNode)
    }
    
    override func layoutSpecThatFits(constrainedSize: ASSizeRange) -> ASLayoutSpec {
        
        let emptyCentered = ASCenterLayoutSpec(horizontalPosition: .Center,
                                               verticalPosition: .Center,
                                               sizingOption: .Default,
                                               child: emptyNode)
        
        let errorCentered = ASCenterLayoutSpec(horizontalPosition: .Center,
                                               verticalPosition: .Center,
                                               sizingOption: .Default,
                                               child: errorNode)

        return ASOverlayLayoutSpec(child: emptyCentered, overlays: [errorCentered, tableNode])
    }
    
}

extension ASOverlayLayoutSpec {
    
    convenience init(child:ASLayoutable, overlays:[ASLayoutable]) {
     
        var children = overlays
        guard let c0 = children.first else {
            self.init(child: child, overlay:nil)
            return
        }
        
        
        
        let initial = ASOverlayLayoutSpec(child: child, overlay: c0)
        
        children.removeFirst()
        
        guard let cLast = children.last else {
            self.init(child:child, overlay: c0)
            return
        }
        
        children.removeLast()
        
        let complete = children.reduce(initial) {
            ASOverlayLayoutSpec(child: $0, overlay: $1)
        }
        
        self.init(child:complete, overlay: cLast)
    }
    
}

@IBDesignable
class PageMetaDataView: UIView {

    override init(frame:CGRect) {
        
        super.init(frame: frame)
    }
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        
        let meta = PageMetaDataNode(data: MZPageMetaData.TheDistanceMetaData())
        
        meta.frame = self.bounds
        addSubview(meta.view)
        
        meta.calculateAndApplyLayout()
        
        print("laid out")
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension ASLayout {
    
    func positionDescription() -> String {
        return "\(position): \(size)"
    }
    
}

extension ASDisplayNode {
    
    func calculateAndApplyLayout() {
        
        let sr = ASSizeRangeMakeExactSize(self.bounds.size)
        calculateAndApplyLayoutThatFits(sr)
    }
    
    func calculateAndApplyLayoutThatFits(constrainedSize:ASSizeRange) {
        
        let fittingLayout = calculateLayoutThatFits(constrainedSize)
    
        applyLayoutToHierarchy(fittingLayout)
    }
    
    func applyLayoutToHierarchy(layout:ASLayout) {
        
        applyLayout(layout)
        
//        guard subnodes.count == layout.sublayouts.count else { return }
        
        for s in 0..<subnodes.count {
            
            let thisSubNode = subnodes[s]
            // let thisSubLayout = layout.sublayouts[s]
            
            thisSubNode.calculateAndApplyLayoutThatFits(ASSizeRangeMakeExactSize(self.bounds.size))
        }
    }
    
    func applyLayout(layout:ASLayout) {
        
        let invalidPosition = isnan(layout.position.x) || isnan(layout.position.y)
        
        let nodePosition = invalidPosition ? CGPointMake(layout.size.width * 0.5, layout.size.height * 0.5) : layout.position
        
        position = nodePosition
        bounds = CGRectMake(0,0, layout.size.width, layout.size.height)
        print(layout.positionDescription())
    }

}

class PageMetaDataNode: ASCellNode {
    
    var details:MZPageMetaData {
        didSet {
            updateNodes()
        }
    }
    
    let titleNode = MetaDataNode(title: "PAGE TITLE")
    let URLNode = MetaDataNode(title: "CANONICAL URL")
    let descriptionNode = MetaDataNode(title: "META DESCRIPTION")
    let h1Node = MetaDataNode(title: "H1")
    let h2Node = MetaDataNode(title: "H2")
    
    init(data:MZPageMetaData) {
        
        details = data
        
        super.init()
        
        for node in [titleNode, URLNode, descriptionNode, h1Node, h2Node] {
            addSubnode(node)
        }
        
        self.backgroundColor = UIColor.whiteColor()
        
        updateNodes()
    }
    
    func updateNodes() {
        titleNode.count = details.htmlMetaData.titleCharacterCount ?? 0
        titleNode.value = details.htmlMetaData.title ?? "-"
        
        URLNode.count = details.htmlMetaData.canonicalURLCharacterCount ?? 0
        URLNode.value = details.htmlMetaData.canonicalURL?.absoluteString ?? ""
    }
    
    override func layoutSpecThatFits(constrainedSize: ASSizeRange) -> ASLayoutSpec {
        
        let vStack = ASStackLayoutSpec(direction: .Vertical,
                                       spacing: 8.0,
                                       justifyContent: .SpaceBetween,
                                       alignItems: .Stretch,
                                       children: [
                                        titleNode,
                                        URLNode,
                                        descriptionNode,
                                        h1Node,
                                        h2Node
            ])
        
        return ASInsetLayoutSpec(insets: UIEdgeInsetsMake(8,15,8,8), child: vStack)
    }
}

class MetaDataNode: ASDisplayNode {
    
    let title:String
    var count:Int = 0 {
        didSet {
            countNode.attributedString = NSAttributedString(string: String(count))
        }
    }
    
    
    var value:String = "" {
        didSet {
            valueNode.attributedString = NSAttributedString(string: value)
        }
    }
    
    let titleNode = ASTextNode()
    let countNode = ASTextNode()
    let valueNode = ASTextNode()
    
    init(title:String) {
        self.title = title
        
        super.init()
        
        addSubnode(titleNode)
        addSubnode(countNode)
        addSubnode(valueNode)
        
        titleNode.attributedString = NSAttributedString(string: title)
    }
    
    override func layoutSpecThatFits(constrainedSize: ASSizeRange) -> ASLayoutSpec {
        
        let headingSpec = ASStackLayoutSpec(direction: .Horizontal,
                                             spacing: 8.0,
                                             justifyContent: .SpaceBetween,
                                             alignItems: .Stretch,
                                             children: [titleNode, countNode])
        
        let contentSpec = ASStackLayoutSpec(direction: .Vertical,
                                      spacing: 4.0,
                                      justifyContent: .SpaceBetween,
                                      alignItems: .Stretch,
                                      children: [headingSpec, valueNode])
        
        return ASInsetLayoutSpec(insets: UIEdgeInsetsMake(8, 15, 8, 8), child: contentSpec)
    }
    
}

